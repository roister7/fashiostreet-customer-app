import Home from './pages/Home';
import Wishlist from './pages/Wishlist';
import Hdorders from './pages/Hdorders';
import Hdcart from './pages/Hdcart';
import Account from './pages/Account';
import Address from './pages/Address';
import Profile from './pages/Profile';
import Location from './pages/Location';
import Search from './pages/Search';
import Login from './pages/Login';
import Signup from './pages/Signup';
import Map from './pages/Map';
import Confirmdelivery from './pages/Confirmdelivery';
import Categorymen from './pages/Categorymen';
import Categorywomen from './pages/Categorywomen';
import Categorykids from './pages/Categorykids';
import Categorywatch from './pages/Categorywatch';
import Categoryfoot from './pages/Categoryfoot';
import Selectaddress from './pages/Selectaddress';
import Shop from './pages/Shop';
import Products from './pages/Products';
import Productdetails from './pages/Productdetails';
import Shophome from './pages/Shophome';
import NotFoundPage from './pages/not-found.vue';

export default [
  {
    path: '/',
    component: Home
  },
  {
    path: '/location/',
    component: Location
  },
  {
    path: '/shop/',
    component: Shop
  },
  {
    path: '/products/',
    component: Products
  },
  {
    path: '/productdetails/',
    component: Productdetails
  },
  {
    path: '/search/',
    component: Search
  },
  {
    path: '/categorymen/',
    component: Categorymen
  },
  {
    path: '/categorywomen/',
    component: Categorywomen
  },
  {
    path: '/categorykids/',
    component: Categorykids
  },
  {
    path: '/categorywatch/',
    component: Categorywatch
  },
  {
    path: '/categoryfoot/',
    component: Categoryfoot
  },
  {
    path: '/wishlist/',
    component: Wishlist
  },
  {
    path: '/hdorders/',
    component: Hdorders
  },
  {
    path: '/hdcart/',
    component: Hdcart
  },
  {
    path: '/login/',
    component: Login
  },
  {
    path: '/signup/',
    component: Signup
  },
  {
    path: '/map/',
    component: Map
  },
  {
    path: '/confirmdelivery/',
    component: Confirmdelivery
  },
  {
    path: '/account/',
    component: Account
  },
  {
    path: '/address/',
    component: Address
  },
  {
    path: '/selectaddress/',
    component: Selectaddress
  },
  {
    path: '/profile/',
    component: Profile
  },
  {
    path: '/shop/shophome/',
    component: Shophome
  },
  {
    path: '(.*)',
    component: NotFoundPage
  }
];
